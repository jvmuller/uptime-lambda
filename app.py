import os
import json
import requests

from chalice import Chalice, Cron
from chalice.app import CloudWatchEvent
from datetime import datetime
from discord_webhook import DiscordWebhook, DiscordEmbed
from http import HTTPStatus
from typing import Union

app = Chalice(app_name='uptime')
requests.packages.urllib3.disable_warnings()


@app.route('/current-status')
def route_current_status() -> dict:
    """Rota para visualizar as responses verificadas pelo Uptime referente ao horário atual."""
    domains = _open_json_file('domains.json')
    
    if domains:
        new_responses = json.dumps([_health_check_domain(domain) for domain in domains])
        _overwrite_responses_file(new_responses)
        return new_responses
    return dict(code='NotFoundDomains', message='Não foram encontrados domínios para serem verificados.')

@app.route('/last-status')
def route_last_status() -> dict:
    """Rota para visualizar as últimas responses verificadas pelo Uptime."""
    last_responses = _open_json_file('last_responses.json')
    
    if last_responses:
        return last_responses
    return dict(code='NotFoundLastResponses', message='Não foram encontrados os últimos status.')

@app.schedule(Cron('0/30', '*', '*', '*', '*', '*'))
def periodic_task_health_check_every_30_seconds(event: CloudWatchEvent):
    """Task períodica que executa todo minuto para verificar se os domínios observados estão bem de saúde."""
    domains = _open_json_file('domains.json')
    
    if domains:
        _overwrite_responses_file(json.dumps([_health_check_domain(domain) for domain in domains]))

def _health_check_domain(current_domain: dict) -> dict:
    """Retorna o response de um domínio após verificar a saúde da disponibilidade do endpoint."""
    current_response = _current_response(current_domain)
    last_responses = _open_json_file('last_responses.json')
    
    if last_responses:
        for last_response in last_responses:
            current_response = _set_offline_date_to_current_response(current_response, last_response)
            if _service_is_down(current_response, last_response):
                _send_to_discord_webhook(_template_discord_webhook(current_response)['OFFLINE'])
                
            if _service_is_up(current_response, last_response):
                current_response['offline_date'] = None
                _send_to_discord_webhook(_template_discord_webhook(current_response)['ONLINE'])
    
    return current_response

def _set_offline_date_to_current_response(current_response: dict, last_response: dict) -> dict:
    """Retorna o dicionário de current_response com o valor do offline_date do last_response."""
    if current_response['name'] == last_response['name'] and last_response['offline_date']:
        current_response['offline_date'] = last_response['offline_date']
    return current_response

def _template_discord_webhook(current_response: dict) -> dict:
    """Retorna dicionário com os valores necessários para montar o embed da mensagem enviada no Discord Webhook."""
    offline_time = '0 seconds'
    if current_response['offline_date']:
        time = datetime.now() - datetime.strptime(current_response['offline_date'], '%Y-%m-%d %H:%M:%S')
        minutes = int(time.total_seconds() / 60)
        seconds = int(time.total_seconds() % 60)
        offline_time = f'{minutes} minutes and {seconds} seconds'
    
    return {
        'OFFLINE': dict(
            title=f'Service {current_response["name"]} is down! Reason: HTTP {current_response["status_code"]} - {HTTPStatus(current_response["status_code"]).phrase}', 
            description=f'{HTTPStatus(current_response["status_code"]).description}.', 
            date=current_response['date'],
            url=current_response['url'],
            color='db2e2e'
        ),
        'ONLINE': dict(
            title=f'Service {current_response["name"]} is up!', 
            description=f'It was down for about {offline_time}.',
            date=current_response['date'],
            url=current_response['url'],
            color='04b34f'
        ),
    }

def _check_if_can_use_discord_webhook() -> bool:
    """"Verifica se a variável de ambiente USE_DISCORD_WEBHOOK (string) é equivalente ao valor de True."""
    if os.environ['USE_DISCORD_WEBHOOK'].upper() == 'TRUE' or os.environ['USE_DISCORD_WEBHOOK'] == '1':
        return True
    return False

def _send_to_discord_webhook(template: dict):
    """Envia uma mensagem através do Discord Webhook informando a saúde da aplicação."""
    if _check_if_can_use_discord_webhook():
        webhook = DiscordWebhook(url=os.environ['DISCORD_WEBHOOK_URL'])
        embed = DiscordEmbed(title=template['title'], description=template['description'], color=template['color'])
        embed.set_author(name='ChaliceUptime', icon_url='https://raw.githubusercontent.com/aws/chalice/1.27.1/docs/source/img/chalice-logo-icon-whitespace.png')
        embed.add_embed_field(name='Date',value=template['date'])
        embed.add_embed_field(name='URL',value=template['url'])
        webhook.add_embed(embed)
        webhook.execute(embed)

def _open_json_file(filename: str) -> Union[list, bool]:
    """Retorna um dicionário caso consiga carregar o arquivo JSON. Retorna falso caso não seja possível."""
    try:
        return json.load(open(filename, mode='r', encoding='utf-8'))
    except json.JSONDecodeError:
        return False

def _current_response(current_domain: dict) -> dict:
    """Retorna um dicionário após obter o status code do endpoint do domínio informado."""
    try:
        response = requests.get(current_domain['url'], verify=False)
        status_code = response.status_code
    except requests.ConnectionError:
        status_code = 502
    
    date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    offline_date = date if status_code != 200 else None
    return dict(status_code=status_code, date=date, offline_date=offline_date, **current_domain)

def _service_is_down(current_response: dict, last_response: dict):
    """Retorna um booleano após verificar se o serviço ficou offline."""
    if current_response['name'] == last_response['name'] and current_response['status_code'] != 200 and last_response['status_code'] == 200:
        print(f"SERVICE {current_response['url']} IS DOWN!")
        return True
    return False
  
def _service_is_up(current_response: dict, last_response: dict) -> bool:
    """Retorna um booleano após verificar se o serviço ficou online."""
    if current_response['name'] == last_response['name'] and current_response['status_code'] == 200 and last_response['status_code'] != 200:
        print(f"SERVICE {current_response['url']} IS UP!")
        return True
    return False  

def _overwrite_responses_file(new_responses: dict):
    """Sobrescreve o arquivo last_responses.json com novos dados."""
    file = open('last_responses.json', mode='w', encoding='utf-8')
    file.write(new_responses)
    file.close()
